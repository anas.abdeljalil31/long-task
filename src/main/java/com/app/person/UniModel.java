package com.app.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="university") 
public class UniModel implements Serializable{

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="uni_id")
	private Integer id;

	@Column(name="name")
	private String name;

	@Column(name="location")
	private String location;
	
	@OneToMany(mappedBy = "university")
	private List<StudModel> students;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<StudModel> getStudents() {
		return students;
	}

	public void setStudents(List<StudModel> students) {
		this.students = students;
	}


}