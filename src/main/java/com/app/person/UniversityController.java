package com.app.person;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.jk.webstack.controllers.JKManagedBeanWithOrmSupport;

@ManagedBean(name = "mbUniversity")
@ViewScoped
public class UniversityController extends JKManagedBeanWithOrmSupport<UniModel> {
}