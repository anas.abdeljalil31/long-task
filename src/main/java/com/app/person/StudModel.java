package com.app.person;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="student") 
public class StudModel implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="student_id")
	private Integer id;

	@Column(name="name")
	private String name;

	@Column(name="major")
	private String major;

	@ManyToOne
	@JoinColumn(name="uni_id", nullable = false)
	//@Column(name="university")
	private UniModel university;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public UniModel getUniversity() {
		return university;
	}

	public void setUniversity(UniModel university) {
		this.university = university;
	}
	
	
}